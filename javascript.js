var audio = new Map();
var mapNotes =  new Map();
mapNotes = {
    a: "Re",
    s: "Mi",
    d: "Fa",
    f: "Sol",
    g: "La",
    h: "Si",
    j: "Do",
};

window.addEventListener('load', () =>{
    document.addEventListener('mousedown', playSound);
    document.addEventListener('keydown', playSoundWithKey);
    document.addEventListener('mouseup', pauseClick);
    document.addEventListener('keyup', pause);
    
})



function playSound(event){
    clickMouse = new Audio(event.target.id + ".aac");
    clickMouse.play();
}
function playKeySound(key){
    console.log(mapNotes[key]);
    if (!audio.has(key)) {
        audio.set(key, new Audio(mapNotes[key] + ".aac"));
    }
    audio.get(key).play();
    
}


function playSoundWithKey(event) {
    if (event.repeat == false) {
        playKeySound(event.key);
    }

}

function pause(event){
    audio.get(event.key).pause();
    audio.delete(event.key);
}
function pauseClick(){
    clickMouse.pause();
}

function openNav() {
    document.getElementById("mySidebar").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
}